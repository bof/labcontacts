package com.codepath.examples.contactloader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

//https://github.com/rogcg/gridview-autoresized-images-sample
public class GridActivity extends Activity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);

        GridView gridView = (GridView)findViewById(R.id.gridview);
        gridView.setAdapter(new MyAdapter(this));
    }

    private class MyAdapter extends BaseAdapter
    {
        private List<Item> items = new ArrayList<Item>();
        private LayoutInflater inflater;

        public MyAdapter(Context context)
        {
            inflater = LayoutInflater.from(context);

            items.add(new Item("Bill Maher", R.drawable.p1));
            items.add(new Item("Some Dude", R.drawable.p2));
            items.add(new Item("Another person", R.drawable.p3));
            items.add(new Item("Another person", R.drawable.p4));
            items.add(new Item("Another person", R.drawable.p5));
            items.add(new Item("Another person", R.drawable.p6));
            items.add(new Item("Another person", R.drawable.p7));
            items.add(new Item("Another person", R.drawable.p8));
            items.add(new Item("Another person", R.drawable.p9));
            items.add(new Item("Another person", R.drawable.p10));
            items.add(new Item("Bill Maher", R.drawable.p1));
            items.add(new Item("Some Dude", R.drawable.p2));
            items.add(new Item("Another person", R.drawable.p3));
            items.add(new Item("Another person", R.drawable.p4));
            items.add(new Item("Another person", R.drawable.p5));
            items.add(new Item("Another person", R.drawable.p6));
            items.add(new Item("Another person", R.drawable.p7));
            items.add(new Item("Another person", R.drawable.p8));
            items.add(new Item("Another person", R.drawable.p9));
            items.add(new Item("Another person", R.drawable.p10));
            items.add(new Item("Bill Maher", R.drawable.p1));
            items.add(new Item("Some Dude", R.drawable.p2));
            items.add(new Item("Another person", R.drawable.p3));
            items.add(new Item("Another person", R.drawable.p4));
            items.add(new Item("Another person", R.drawable.p5));
            items.add(new Item("Another person", R.drawable.p6));
            items.add(new Item("Another person", R.drawable.p7));
            items.add(new Item("Another person", R.drawable.p8));
            items.add(new Item("Another person", R.drawable.p9));
            items.add(new Item("Another person", R.drawable.p10));


        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int i)
        {
            return items.get(i);
        }

        @Override
        public long getItemId(int i)
        {
            return items.get(i).drawableId;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup)
        {
            View v = view;
            ImageView picture;
            TextView name;

            if(v == null)
            {
               v = inflater.inflate(R.layout.gridview_item, viewGroup, false);
               v.setTag(R.id.picture, v.findViewById(R.id.picture));
               v.setTag(R.id.text, v.findViewById(R.id.text));
            }

            picture = (ImageView)v.getTag(R.id.picture);
            name = (TextView)v.getTag(R.id.text);

            Item item = (Item)getItem(i);

            picture.setImageResource(item.drawableId);
            name.setText(item.name);

            return v;
        }

        private class Item
        {
            final String name;
            final int drawableId;

            Item(String name, int drawableId)
            {
                this.name = name;
                this.drawableId = drawableId;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.grid, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_contacts){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();

        }

        //else it's action_loader
        else {

            Intent intent = new Intent(this, LoaderActivity.class);
            startActivity(intent);
            finish();

        }

        return super.onOptionsItemSelected(item);
    }

}
